#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 tinyproxy <tinyproxy@gmail.com>
#
# Distributed under terms of the MIT license.

# (╯‵□′)╯︵┻━┻

"""
"""

import os
import tempfile
import requests
import sys
import zipfile
import StringIO
import logging

def main(url, destpath, *args):
    logging.critical("%s, %s", url, destpath)
    response = requests.get(url, stream=True)
    zip_content = response.raw.read()
    zip_file_obj = StringIO.StringIO()
    zip_file_obj.write(zip_content)
    zip = zipfile.ZipFile(zip_file_obj)
    tempdir = tempfile.mkdtemp()
    zip.extractall(tempdir)
    root_dir = os.path.join(tempdir, os.listdir(tempdir)[0])
    if os.path.exists(destpath) is False:
        os.makedirs(destpath) # mkdir -p ${destpath} to avoid I/O error
    os.rename(root_dir, destpath)

if __name__ == "__main__":
    ret = main(*sys.argv[1:])
    exit(ret)
